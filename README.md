# Spring Boot - API REST

## Nouveautés 

- Veille
- Ajout de Author et Category (+ ajout des relations)
- Ajout de BookService
- Ajout des erreurs et suppression des autowired
- Ajout des données de test pour POSTMAN (endpoint : books/category et books/author)

## Veille 

### Qu'est-ce qu'un ORM ?

Un Object Relational Mapping (ORM) est un outil qui permet de créer une correspondance entre les objets d'un programme informatique et les tables d'une base de données relationnelle. Son objectfi est de convertir les données entre les deux formats, ce qui permet aux développeurs de travailler avec des objets plutôt qu'avec des lignes de table.

### Qu'est-ce que Hibernate ?

Hibernate est en fait une implémentation d'un ORM. Il fait donc la même chose que l'ORM décrit avant mais avec des fonctionnalités supplémentaires comparé à d'autres implémentations d'ORM.


### Qu'est-ce que Spring Data JPA ?

Spring Data JPA est une couche supplémentaire qui simplifie l'utilisation d'ORM tels que Hibernate dans les applications Spring Boot. 
Spring Data JPA fournit une interface simple pour effectuer des opérations CRUD sur les entités, ce qui permet d'interagir facilement avec la base de données.
Cela permet d'éviter d'écrire du code SQL brut (donc simplifie le code et la maintenabilité).


### Qu'est-ce que JDBC ?

JDBC (Java Database Connectivity) est une bibliothèque qui permet de faire la connexion avec la base de données relationnelle. Il s'agit d'une technologie de plus bas niveau que Spring Data JPA ce qui veut dire qu'il faut écrire du code SQL brut. Son avantage est donc d'offrir plus de flexibilité que Spring Data JPA.  
Hibernate va utiliser JDBC pour se connecter à la base de données et exécuter des requêtes SQL. Cependant, Hibernate fournit une couche d'abstraction supplémentaire qui permet aux développeurs de travailler avec des objets plutôt qu'avec des lignes de table. 

En fonction du projet, il est donc possible de choisir soit JDBC soit Spring Data JPA selon les besoins. Il est également possible d'utiliser les deux dans un même projet en utilisant JDBC pour certaines tâches spécifiques qui nécessitent une plus grande flexibilité, et Spring Data JPA pour les opérations CRUD standard. 

## Configuration


Configurez le fichier `src/main/resources/application.properties` avec les informations de connexion à votre base de données PostgreSQL :

   ```
   spring.datasource.url=jdbc:postgresql://localhost:5432/jpalibrary
   spring.datasource.username=votre_utilisateur
   spring.datasource.password=votre_mot_de_passe
   spring.jpa.hibernate.ddl-auto=update
   ```

## Lancer l'application

Exécutez l'application en utilisant la commande `mvn spring-boot:run`. L'application démarrera sur le port `8080` par défaut.

## Initialiser avec des livres

Avec POSTMAN

### Ajouter deux livres

1. Première fois :

   ```
   POST localhost:8080/books
   ```

   Avec le body suivant :

   ```json
   {
     "title": "Livre 1",
     "description": "Description",
     "available": true,
     "author" : {
         "firstName" : "authorName",
         "lastName" : "authorLastName"
     },
     "category" : {
        "name" : "categoryDuLivre"
     }
   }
   ```

2. Deuxième fois :

   ```
   POST localhost:8080/books
   ```

   Avec le body suivant :

   ```json
   {
     "title": "Livre 2",
     "available": false,
          "author" : {
         "firstName" : "authorName",
         "lastName" : "authorLastName"
     },
     "category" : {
        "name" : "categoryDuLivre"
     }
   }
   ```

### Vérifier si les livres ont été ajoutés

1. Récupérer tous les livres :

   ```
   GET localhost:8080/books
   ```

2. Récupérer un livre spécifique :

   ```
   GET localhost:8080/books/1
   ```

### Mettre à jour un livre

1. Mettre à jour le premier livre :

   ```
   PUT localhost:8080/books/1
   ```

   Avec le body suivant :

   ```json
   {
     "title": "Mise à jour Livre 1",
     "description": "Description du livre 1 mise à jour",
     "available": false
   }
   ```

2. Vérifier si le livre a été mis à jour :

   ```
   GET localhost:8080/books/1
   ```

### Supprimer un livre

1. Supprimer le premier livre :

   ```
   DELETE localhost:8080/books/1
   ```

2. Vérifier si le livre a été supprimé :

   ```
   GET localhost:8080/books
   ```

### Rechercher par titre un livre 

1. Créer un livre avec un titre facile 

   ```
   POST localhost:8080/books
   ```

   Avec le body suivant :

   ```json
   {
     "title": "Li",
     "description": "Livre",
     "available": false,
         "category": {
            "name": "Polar"
        },
        "author": {
            "firstName": "Test",
            "lastName": "Test"
        }
   }
   ```

2. Rechercher par titre 

   ```
    GET localhost:8080/books/title/Li
   ```


### Récupèrer liste des auteurs  

   ```
   GET http://localhost:8080/books/author
   ```

### Récupèrer liste des auteurs  

   ```
   GET http://localhost:8080/books/category
   ```