package com.example.API.REST.service;

import com.example.API.REST.entity.Author;
import com.example.API.REST.entity.Book;
import com.example.API.REST.entity.Category;
import com.example.API.REST.repository.AuthorRepository;
import com.example.API.REST.repository.BookRepository;
import com.example.API.REST.repository.CategoryRepository;

import jakarta.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final CategoryRepository categoryRepository;

    public BookService(BookRepository bookRepository, AuthorRepository authorRepository,
            CategoryRepository categoryRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
        this.categoryRepository = categoryRepository;
    }

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public List<Author> getAllAuthor() {
        return authorRepository.findAll();
    }

    public List<Category> getAllCategory() {
        return categoryRepository.findAll();
    }

    public Book getBookById(Long id) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if (optionalBook.isPresent()) {
            return optionalBook.get();
        } else {
            throw new EntityNotFoundException("No book found with id");
        }
    }

    public Book createBook(Book book) {
        // Auteur
        Author author = book.getAuthor();
        Author existingAuthor = authorRepository.findByFirstNameAndLastName(author.getFirstName(),
                author.getLastName());
        if (existingAuthor != null) {
            book.setAuthor(existingAuthor);
        } else {
            Author newAuthor = new Author();
            newAuthor.setFirstName(author.getFirstName());
            newAuthor.setLastName(author.getLastName());
            Author savedAuthor = authorRepository.save(newAuthor);
            book.setAuthor(savedAuthor);
        }
        // Category
        Category category = book.getCategory();
        Category existingCategory = categoryRepository.findByName(category.getName());
        if (existingCategory != null) {
            book.setCategory(existingCategory);
        } else {
            Category newCategory = new Category();
            newCategory.setName(category.getName());
            Category savedCategory = categoryRepository.save(category);
            book.setCategory(savedCategory);
        }
        return bookRepository.save(book);
    }

    public Book updateBook(Long id, Book updatedBook) {
        return bookRepository.findById(id).map(book -> {
            book.setTitle(updatedBook.getTitle());
            book.setDescription(updatedBook.getDescription());
            book.setAvailable(updatedBook.getAvailable());
            return bookRepository.save(book);
        }).orElseThrow(() -> new RuntimeException("Book not found with id " + id));
    }

    public void deleteBook(Long id) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if (optionalBook.isPresent()) {
            bookRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException("No book found with this id" + id);
        }
    }

    public Book findBookByTitle(String title) {
        Book book = bookRepository.findByTitle(title);
        if (book == null) {
            throw new EntityNotFoundException("No book found with title " + title);
        }
        return book;
    }
}
