package com.example.API.REST.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.API.REST.entity.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
    Book findByTitle(String title);
}
